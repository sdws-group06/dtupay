Feature: Bank Account Number Request
  Scenario: Bank Account Numbers retrieved successfully
    Given a customer has been registered
    Given a merchant has been registered
    When the "BankAccountNumbersRequested" event is received
    Then the "BankAccountNumbersAssigned" event is dispatched



