Feature: Account Creation
  Scenario: Customer is successfully registered
    When a "CustomerRegistrationRequested" event is received with bank account number "123"
    Then the "CustomerRegistered" event is sent

  Scenario: Customer with the given bank number already exists
    Given the account with the bank account number "123" already exists
    When a "CustomerRegistrationRequested" event is received with bank account number "123"
    Then the "CustomerAlreadyRegistered" event sends the message "An account with this bank account number is already registered"

  Scenario: Merchant is successfully registered
    When a "MerchantRegistrationRequested" event is received with bank account number "431"
    Then the "MerchantRegistered" event is sent

  Scenario: Merchant with the given bank number already exists
    Given the account with the bank account number "431" already exists
    When a "MerchantRegistrationRequested" event is received with bank account number "431"
    Then the "MerchantAlreadyRegistered" event sends the message "An account with this bank account number is already registered"
