package com.dtupay.commands;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Sergio Segura (s194726)
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RegisterAccountCommand {
    private String firstName;
    private String lastName;
    private String accountNumber;
}
