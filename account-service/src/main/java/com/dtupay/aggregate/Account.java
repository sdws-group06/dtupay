package com.dtupay.aggregate;

import com.dtupay.commands.RegisterAccountCommand;
import com.dtupay.events.AccountEvent;
import com.dtupay.events.AccountRegisteredEvent;
import lombok.Getter;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * @author: Khaled Zamzam (s195487)
 */
@Getter
public class Account {
    private UUID id;
    private String firstName;
    private String lastName;
    private String bankAccountNumber;
    private final List<AccountEvent> appliedEvents = new ArrayList<>();
    private final Map<Class<? extends AccountEvent>, Consumer<AccountEvent>> handlers = new HashMap<>();

    public Account() {
        registerEventHandlers();
    }

    public static Account create(RegisterAccountCommand command) {
        UUID id = UUID.randomUUID();
        AccountRegisteredEvent event = new AccountRegisteredEvent(id, command.getFirstName(), command.getLastName(), command.getAccountNumber());
        Account account = new Account();
        account.id = id;
        account.appliedEvents.add(event);
        return account;
    }

    public static Account createFromEvents(List<AccountEvent> events) {
        Account account = new Account();
        account.applyEvents(events.stream());
        return account;
    }

    private void registerEventHandlers() {
        this.handlers.put(AccountRegisteredEvent.class, e -> apply((AccountRegisteredEvent) e));
    }

    public void clearAppliedEvents() {
        appliedEvents.clear();
    }

    private void applyEvents(Stream<AccountEvent> events) {
        events.forEachOrdered(this::applyEvent);
    }

    private void applyEvent(AccountEvent e) {
        handlers.getOrDefault(e.getClass(), this::missingHandler).accept(e);
    }

    private void missingHandler(AccountEvent e) {
        throw new Error("Handler for event " + e + " missing");
    }

    private void apply(AccountRegisteredEvent event) {
        id = event.getId();
        firstName = event.getFirstName();
        lastName = event.getLastName();
        bankAccountNumber = event.getBankAccountNumber();
    }
}

