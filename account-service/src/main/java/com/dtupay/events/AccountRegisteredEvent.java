package com.dtupay.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.UUID;

/**
 * @author: Khaled Zamzam (s195487)
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class AccountRegisteredEvent extends AccountEvent {
    private static final long serialVersionUID = -3654805941906491808L;
    private UUID id;
    private String firstName;
    private String lastName;
    private String bankAccountNumber;
}
