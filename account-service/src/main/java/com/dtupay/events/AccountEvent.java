package com.dtupay.events;

import lombok.Getter;
import java.io.Serializable;

/**
 * @author: Sergio Segura (s194726)
 */
public abstract class AccountEvent implements Serializable {
    private static final long serialVersionUID = 5219868711031429923L;

    private static long versionCount = 1;

    @Getter
    private final long version = versionCount++;
}
