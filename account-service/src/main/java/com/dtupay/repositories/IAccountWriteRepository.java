package com.dtupay.repositories;

import com.dtupay.exceptions.AccountAlreadyExistsException;
import com.dtupay.aggregate.Account;

/**
 * @author: Khaled Zamzam (s195487)
 */
public interface IAccountWriteRepository {
    void save(Account account) throws AccountAlreadyExistsException;
}
