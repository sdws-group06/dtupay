package com.dtupay.repositories;

import messaging.MessageQueue;
import com.dtupay.aggregate.Account;

/**
 * @author: Sergio Segura (s194726)
 */
public class InMemoryAccountWriteRepository implements IAccountWriteRepository {
    private final EventStore store;

    public InMemoryAccountWriteRepository(MessageQueue eventBus) {
        this.store =  new EventStore(eventBus);
    }

    public void save(Account account) {
        store.addEvents(account.getId(), account.getAppliedEvents());
        account.clearAppliedEvents();
    }
}