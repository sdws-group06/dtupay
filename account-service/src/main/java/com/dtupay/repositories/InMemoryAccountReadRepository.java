package com.dtupay.repositories;

import messaging.Event;
import messaging.MessageQueue;
import com.dtupay.events.AccountRegisteredEvent;
import com.dtupay.exceptions.AccountDoesNotExistException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class InMemoryAccountReadRepository implements IAccountReadRepository {
    private final Map<UUID, String> bankAccountNumbers = new HashMap<>();

    public InMemoryAccountReadRepository(MessageQueue queue) {
        queue.addHandler("AccountRegisteredEvent", this::applyAccountRegisteredEvent);
    }

    @Override
    public String getBankAccountNumber(UUID accountId) throws AccountDoesNotExistException {
        String accountNumber = bankAccountNumbers.get(accountId);

        if(accountNumber == null) throw new AccountDoesNotExistException("Account does not exist");

        return accountNumber;
    }

    @Override
    public UUID getAccountIdByBankAccountNumber(String bankAccountNumber) {
        for(Map.Entry<UUID, String> entry : bankAccountNumbers.entrySet()) {
            if(entry.getValue().equals(bankAccountNumber)) return entry.getKey();
        }
        return null;
    }

    @Override
    public boolean accountNumberExists(String accountNumber) {
        return bankAccountNumbers.containsValue(accountNumber);
    }

    public void applyAccountRegisteredEvent(Event e) {
        AccountRegisteredEvent event = e.getArgument(0, AccountRegisteredEvent.class);
        bankAccountNumbers.put(event.getId(), event.getBankAccountNumber());
    }
}
