package com.dtupay.repositories;

import com.dtupay.events.AccountRegisteredEvent;
import lombok.Getter;
import messaging.Event;
import messaging.MessageQueue;
import com.dtupay.events.AccountEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Khaled Zamzam (s195487)
 */
@Getter
public class EventStore {

    private final Map<UUID, List<AccountEvent>> store = new ConcurrentHashMap<>();
    private final MessageQueue eventBus;

    public EventStore(MessageQueue eventBus) {
        this.eventBus = eventBus;
    }

    public void addEvent(UUID id, AccountEvent event) {
        if(!store.containsKey(id)) {
            store.put(id, new ArrayList<>());
        }
        store.get(id).add(event);
        
        Event e = null;

        if(event instanceof AccountRegisteredEvent) {
            e = new Event("AccountRegisteredEvent", new Object[] { event });
        }

        eventBus.publish(e);
    }

    public void addEvents(UUID id, List<AccountEvent> events) {
        events.forEach(e -> addEvent(id, e));
    }

    public List<AccountEvent> getEventsFor(UUID id) {
        if(!store.containsKey(id)) return null;
        return store.get(id);
    }
}