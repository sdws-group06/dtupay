package com.dtupay.repositories;

import com.dtupay.exceptions.AccountDoesNotExistException;
import java.util.UUID;

/**
 * @author: Sergio Segura (s194726)
 */
public interface IAccountReadRepository {
    String getBankAccountNumber(UUID accountId) throws AccountDoesNotExistException;
    UUID getAccountIdByBankAccountNumber(String bankAccountNumber);
    boolean accountNumberExists(String accountNumber);
}
