package com.dtupay.services;

import com.dtupay.exceptions.AccountAlreadyExistsException;
import com.dtupay.exceptions.AccountDoesNotExistException;
import com.dtupay.repositories.IAccountReadRepository;
import messaging.Event;
import messaging.MessageQueue;
import com.dtupay.valueobjects.CorrelationId;
import com.dtupay.aggregate.Account;
import com.dtupay.commands.RegisterAccountCommand;
import com.dtupay.repositories.IAccountWriteRepository;
import java.util.UUID;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class AccountService {
    private final IAccountWriteRepository repository;
    private final IAccountReadRepository readRepository;
    private final MessageQueue queue;

    public AccountService(MessageQueue queue, IAccountWriteRepository repository, IAccountReadRepository readRepository) {
        this.queue = queue;
        this.repository = repository;
        this.readRepository = readRepository;
        this.queue.addHandler("CustomerRegistrationRequested", this::handleCustomerRegistration);
        this.queue.addHandler("MerchantRegistrationRequested", this::handleMerchantRegistration);
        this.queue.addHandler("BankAccountNumbersRequested", this::handleBankNumbersRequested);
    }

    public UUID createAccount(RegisterAccountCommand command) throws AccountAlreadyExistsException {
        if(readRepository.accountNumberExists(command.getAccountNumber())) {
            throw new AccountAlreadyExistsException("An account with this bank account number is already registered");
        }
        Account account = Account.create(command);
        repository.save(account);
        return account.getId();
    }

    public String getBankAccountNumberById(UUID id) throws AccountDoesNotExistException {
        String bankAccountNumber = readRepository.getBankAccountNumber(id);
        if(bankAccountNumber == null) throw new AccountDoesNotExistException("Account does not exist");
        return bankAccountNumber;
    }

    public void handleCustomerRegistration(Event e) {
        RegisterAccountCommand command = e.getArgument(0, RegisterAccountCommand.class);
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);
        try {
            UUID id = createAccount(command);
            Event event = new Event("CustomerRegistered", new Object[] { id, correlationId });
            queue.publish(event);
        } catch (AccountAlreadyExistsException exception) {
            Event event = new Event("CustomerAlreadyRegistered", new Object[] { exception.getMessage(), correlationId });
            queue.publish(event);
        }
    }

    public void handleMerchantRegistration(Event e) {
        RegisterAccountCommand command = e.getArgument(0, RegisterAccountCommand.class);
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);
        try {
            UUID id = createAccount(command);
            Event event = new Event("MerchantRegistered", new Object[] { id, correlationId });
            queue.publish(event);
        } catch (AccountAlreadyExistsException exception) {
            Event event = new Event("MerchantAlreadyRegistered", new Object[] { exception.getMessage(), correlationId });
            queue.publish(event);
        }
    }

    public void handleBankNumbersRequested(Event e) {
        String customerId = e.getArgument(0, String.class);
        String merchantId = e.getArgument(1, String.class);
        UUID paymentId = e.getArgument(2, UUID.class);
        CorrelationId correlationId = e.getArgument(3, CorrelationId.class);

        try {
            String customerBankAccountNumber = getBankAccountNumberById(UUID.fromString(customerId));
            String merchantBankAccountNumber = getBankAccountNumberById(UUID.fromString(merchantId));
            Event event = new Event("BankAccountNumbersAssigned", new Object[] { customerBankAccountNumber, merchantBankAccountNumber, paymentId, correlationId });
            queue.publish(event);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }
}
