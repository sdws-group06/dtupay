package com.dtupay.exceptions;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class AccountAlreadyExistsException extends Exception {
    public AccountAlreadyExistsException(String message) {
        super(message);
    }
}
