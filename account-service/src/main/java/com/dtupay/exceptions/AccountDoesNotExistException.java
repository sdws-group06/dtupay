package com.dtupay.exceptions;

/**
 * @author: Sergio Segura (s194726)
 */
public class AccountDoesNotExistException extends Exception {
    public AccountDoesNotExistException(String message) {
        super(message);
    }
}
