package com.dtupay;

import com.dtupay.repositories.InMemoryAccountReadRepository;
import com.dtupay.repositories.InMemoryAccountWriteRepository;
import messaging.implementations.RabbitMqQueue;
import com.dtupay.repositories.IAccountReadRepository;
import com.dtupay.repositories.IAccountWriteRepository;
import com.dtupay.services.AccountService;

/**
 * @author: Sergio Segura (s194726)
 */
public class StartUp
{
    public static void main(String[] args)
    {
        new StartUp().startUp();
    }

    private void startUp() {
        RabbitMqQueue mq = new RabbitMqQueue("rabbitMq");
        IAccountWriteRepository repository = new InMemoryAccountWriteRepository(mq);
        IAccountReadRepository readRepository = new InMemoryAccountReadRepository(mq);
        new AccountService(mq, repository, readRepository);
    }
}
