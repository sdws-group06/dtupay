package com.dtupay;

import com.dtupay.commands.RegisterAccountCommand;
import com.dtupay.exceptions.AccountAlreadyExistsException;
import com.dtupay.exceptions.AccountDoesNotExistException;
import com.dtupay.repositories.InMemoryAccountReadRepository;
import com.dtupay.repositories.InMemoryAccountWriteRepository;
import com.dtupay.repositories.IAccountReadRepository;
import com.dtupay.repositories.IAccountWriteRepository;
import com.dtupay.services.AccountService;
import com.dtupay.valueobjects.CorrelationId;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import messaging.implementations.MessageQueueSync;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author: Sergio Segura (s194726)
 */

public class BankAccountNumberSteps {
    MessageQueue queue = new MessageQueueSync();
    MessageQueue queueExternal = mock(MessageQueue.class);
    IAccountWriteRepository writeRepo = new InMemoryAccountWriteRepository(queue);
    IAccountReadRepository readRepo = new InMemoryAccountReadRepository(queue);
    AccountService service = new AccountService(queueExternal, writeRepo, readRepo);
    UUID cid, mid, paymentId;

    CorrelationId correlationId;

    @Given("a customer has been registered")
    public void aCustomerHasBeenRegistered() throws AccountAlreadyExistsException {
        cid = service.createAccount(new RegisterAccountCommand("John", "Doe", "123"));
    }

    @Given("a merchant has been registered")
    public void aMerchantHasBeenRegistered() throws AccountAlreadyExistsException {
        mid = service.createAccount(new RegisterAccountCommand("Jane", "Smith", "431"));
    }

    @When("the {string} event is received")
    public void theEventIsReceived(String eventName) {
        correlationId = CorrelationId.randomId();
        paymentId = UUID.randomUUID();

        service.handleBankNumbersRequested(new Event(eventName, new Object[] { cid, mid, paymentId, correlationId }));
    }

    @Then("the {string} event is dispatched")
    public void theEventIsDispatched(String eventName) throws AccountDoesNotExistException {
        String merchantBankAccountNumber = service.getBankAccountNumberById(mid);
        String customerBankAccountNumber = service.getBankAccountNumberById(cid);
        Event event = new Event(eventName, new Object[] { customerBankAccountNumber, merchantBankAccountNumber, paymentId, correlationId});
        verify(queueExternal).publish(event);
    }
}
