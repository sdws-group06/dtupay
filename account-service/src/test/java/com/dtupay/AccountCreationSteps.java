package com.dtupay;

import com.dtupay.exceptions.AccountAlreadyExistsException;
import com.dtupay.valueobjects.CorrelationId;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import messaging.implementations.MessageQueueSync;
import com.dtupay.commands.RegisterAccountCommand;
import com.dtupay.repositories.IAccountReadRepository;
import com.dtupay.repositories.IAccountWriteRepository;
import com.dtupay.repositories.InMemoryAccountReadRepository;
import com.dtupay.services.AccountService;
import com.dtupay.repositories.InMemoryAccountWriteRepository;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class AccountCreationSteps {
    MessageQueue queue = new MessageQueueSync();
    MessageQueue queueExternal = mock(MessageQueue.class);
    IAccountWriteRepository writeRepo = new InMemoryAccountWriteRepository(queue);
    IAccountReadRepository readRepo = new InMemoryAccountReadRepository(queue);
    AccountService service = new AccountService(queueExternal, writeRepo, readRepo);
    RegisterAccountCommand command;
    CorrelationId correlationId;
    String accountNumber;

    @When("a {string} event is received with bank account number {string}")
    public void aEventIsReceivedWithBankAccountNumber(String eventName, String bankAccountNumber) {
        accountNumber = bankAccountNumber;
        correlationId = CorrelationId.randomId();
        command = new RegisterAccountCommand("John","Doe",bankAccountNumber);

        if(eventName.equals("CustomerRegistrationRequested")) {
            service.handleCustomerRegistration(new Event(eventName, new Object[] { command, correlationId }));
        } else {
            service.handleMerchantRegistration(new Event(eventName, new Object[] { command, correlationId }));
        }
    }

    @Then("the {string} event is sent")
    public void theEventIsSent(String eventName) {
        UUID createdAccountId = readRepo.getAccountIdByBankAccountNumber(accountNumber);
        Event event = new Event(eventName, new Object[] { createdAccountId, correlationId });
        verify(queueExternal).publish(event);
    }

    @Then("the {string} event sends the message {string}")
    public void theEventSendsTheMessage(String eventName, String message) {;
        Event event = new Event(eventName, new Object[] { message, correlationId });
        verify(queueExternal).publish(event);
    }

    @Given("the account with the bank account number {string} already exists")
    public void theAccountWithTheBankAccountNumberAlreadyExists(String bankAccountNumber) throws AccountAlreadyExistsException {
        service.createAccount(new RegisterAccountCommand("John","Doe", bankAccountNumber));
    }
}
