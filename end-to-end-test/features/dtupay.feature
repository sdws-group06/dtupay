Feature: Payment
  Scenario: Successful Payment
    Given a customer with a bank account with balance 1000
    And that the customer is registered with DTU Pay
    Given a merchant with a bank account with balance 2000
    And that the merchant is registered with DTU Pay
    Given the customer has 5 tokens
    When the customer presents a token
    Then the merchant initiates a payment for 100 kr by the customer
    When the payment is successful
    Then the balance of the customer at the bank is 900 kr
    And the balance of the merchant at the bank is 2100 kr
    When the customer gets his reports
    Then the customer reports contain the new payment
    When the merchant gets his reports
    Then the merchant reports contain the new payment

  Scenario: Not enough amount
    Given a customer with a bank account with balance 1000
    And that the customer is registered with DTU Pay
    Given a merchant with a bank account with balance 2000
    And that the merchant is registered with DTU Pay
    Given the customer has 5 tokens
    When the customer presents a token
    Then the merchant initiates a payment for 3000 kr by the customer
    When the payment is unsuccessful
    Then the balance of the customer at the bank is 1000 kr
    And the balance of the merchant at the bank is 2000 kr

  Scenario: The merchant reuses the same token in a new transaction
    Given a customer with a bank account with balance 1000
    And that the customer is registered with DTU Pay
    Given a merchant with a bank account with balance 2000
    And that the merchant is registered with DTU Pay
    Given the customer has 5 tokens
    When the customer presents a token
    Then the merchant initiates a payment for 200 kr by the customer
    When the payment is successful
    And the merchant reuses the same token
    Then the payment is unsuccessful


