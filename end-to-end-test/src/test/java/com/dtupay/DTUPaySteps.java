package com.dtupay;

import com.dtupay.model.CustomerReport;
import com.dtupay.model.MerchantReport;
import dtu.ws.fastmoney.*;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author: Khaled Zamzam (s195487)
 */

public class DTUPaySteps {
    String cid, mid;
    CustomerAPI customerAPI = new CustomerAPI();
    MerchantAPI merchantAPI = new MerchantAPI();
    String customerAccountNumber, merchantAccountNumber;
    List<String> tokens;
    String token;
    BankService bank = new BankServiceService().getBankServicePort();
    boolean successful;
    List<CustomerReport> customerReports;
    List<MerchantReport> merchantReports;
    double paymentAmount;

    @Given("a customer with a bank account with balance {int}")
    public void aCustomerWithABankAccountWithBalance(int balance) throws BankServiceException_Exception {
        User user = new User();
        user.setCprNumber("2107960776");
        user.setFirstName("Jane");
        user.setLastName("Smith");
        customerAccountNumber = bank.createAccountWithBalance(user, BigDecimal.valueOf(balance));
    }

    @Given("that the customer is registered with DTU Pay")
    public void thatTheCustomerIsRegisteredWithDTUPay() {
        cid = customerAPI.register("Jane", "Smith", customerAccountNumber);
    }

    @Given("a merchant with a bank account with balance {int}")
    public void aMerchantWithABankAccountWithBalance(int balance) throws BankServiceException_Exception {
        User user = new User();
        user.setCprNumber("2406920675");
        user.setFirstName("John");
        user.setLastName("Doe");
        merchantAccountNumber = bank.createAccountWithBalance(user, BigDecimal.valueOf(balance));
    }

    @Given("that the merchant is registered with DTU Pay")
    public void thatTheMerchantIsRegisteredWithDTUPay() {
        mid = merchantAPI.register("John", "Doe", merchantAccountNumber);
    }

    @Given("the customer has {int} tokens")
    public void theCustomerHasTokens(int amount) {
        tokens = customerAPI.getTokens(amount, cid);
    }

    @When("the customer presents a token")
    public void theCustomerPresentsAToken() {
        token = tokens.get(0);
    }

    @Then("the merchant initiates a payment for {int} kr by the customer")
    public void theMerchantInitiatesAPaymentForKrByTheCustomer(int amount) {
        this.paymentAmount = amount;
        successful = merchantAPI.pay(token, mid, amount);
    }

    @When("the payment is successful")
    public void thePaymentIsSuccessful() {
        assertTrue(successful);
    }

    @When("the payment is unsuccessful")
    public void thePaymentIsUnSuccessful() {
        assertFalse(successful);
    }

    @Then("the balance of the customer at the bank is {int} kr")
    public void theBalanceOfTheCustomerAtTheBankIsKr(int balance) throws BankServiceException_Exception {
        Account account = bank.getAccount(customerAccountNumber);
        assertEquals(account.getBalance().intValue(), balance);
    }

    @And("the balance of the merchant at the bank is {int} kr")
    public void theBalanceOfTheMerchantAtTheBankIsKr(int balance) throws BankServiceException_Exception {
        Account account = bank.getAccount(merchantAccountNumber);
        assertEquals(account.getBalance().intValue(), balance);
    }

    @And("the merchant reuses the same token")
    public void theMerchantReusesTheSameToken() {
        successful = merchantAPI.pay(token, mid, 200);
    }

    @After
    public void deleteAccounts() throws BankServiceException_Exception {
        bank.retireAccount(customerAccountNumber);
        bank.retireAccount(merchantAccountNumber);
    }

    @When("the customer gets his reports")
    public void theCustomerGetsHisReports() throws InterruptedException {
        Thread.sleep(3000);
        customerReports = customerAPI.getReports(cid);
    }

    @Then("the customer reports contain the new payment")
    public void theCustomerReportsContainsTheNewPayment() {
        assertTrue(customerReports.contains(new CustomerReport(token, mid, paymentAmount)));
    }

    @When("the merchant gets his reports")
    public void theMerchantGetsHisReports() {
        merchantReports = merchantAPI.getReports(mid);
    }

    @Then("the merchant reports contain the new payment")
    public void theMerchantReportsContainsTheNewPayment() {
        assertTrue(merchantReports.contains(new MerchantReport(token, paymentAmount)));
    }
}
