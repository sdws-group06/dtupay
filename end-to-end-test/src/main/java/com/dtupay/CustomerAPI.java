package com.dtupay;

import com.dtupay.model.CustomerReport;
import com.dtupay.model.DTUPayAccount;
import com.dtupay.model.TokenRequest;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;


/**
 * @author: Khaled Zamzam (s195487)
 */

public class CustomerAPI {
    private WebTarget baseUrl;

    public CustomerAPI() {
        Client client = ClientBuilder.newClient();
        this.baseUrl = client.target("http://localhost:8080/customers");
    }

    public String register(String firstName, String lastName, String customerAccount) {
        DTUPayAccount account = new DTUPayAccount(firstName, lastName, customerAccount);
        Response response = baseUrl
                .request()
                .post(Entity.entity(account, MediaType.APPLICATION_JSON));

        return response.readEntity(String.class);
    }

    public List<String> getTokens(int amount, String cid) {
        TokenRequest tokenRequest = new TokenRequest(amount, cid);
        Response response = baseUrl.path("tokens")
                .request()
                .post(Entity.entity(tokenRequest, MediaType.APPLICATION_JSON));
        return response.readEntity(new GenericType<>(){});
    }

    public List<CustomerReport> getReports(String cid) {
        return baseUrl.path("report/" + cid)
                .request()
                .get(new GenericType<>(){});
    }
}
