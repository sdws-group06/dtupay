package com.dtupay;

import com.dtupay.model.DTUPayAccount;
import com.dtupay.model.MerchantReport;
import com.dtupay.model.PaymentRequest;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;

/**
 * @author: Sergio Segura (s194726)
 */

public class MerchantAPI {
    private WebTarget baseUrl;

    public MerchantAPI() {
        Client client = ClientBuilder.newClient();
        this.baseUrl = client.target("http://localhost:8080/merchants");
    }

    public String register(String firstName, String lastName, String customerAccount) {
        DTUPayAccount account = new DTUPayAccount(firstName, lastName, customerAccount);
        Response response = baseUrl
                .request()
                .post(Entity.entity(account, MediaType.APPLICATION_JSON));

        return response.readEntity(String.class);
    }

    public boolean pay(String token, String mid, int amount) {
        PaymentRequest req = new PaymentRequest(token, mid, amount);
        Response response = baseUrl.path("pay")
                .request()
                .post(Entity.entity(req, MediaType.APPLICATION_JSON));

        return response.getStatus() == Response.Status.OK.getStatusCode();
    }

    public List<MerchantReport> getReports(String mid) {
        return baseUrl.path("report/" + mid)
                .request()
                .get(new GenericType<>(){});
    }
}
