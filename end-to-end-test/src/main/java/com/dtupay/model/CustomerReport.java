package com.dtupay.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Khaled Zamzam (s195487)
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CustomerReport {
    private String token;
    private String merchantId;
    private double amount;
}
