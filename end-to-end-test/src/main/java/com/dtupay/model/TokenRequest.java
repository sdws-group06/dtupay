package com.dtupay.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Khaled Zamzam (s195487)
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TokenRequest {
    int amount;
    String cid;
}
