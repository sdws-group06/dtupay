package com.dtupay.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Sergio Segura (s194726)
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DTUPayAccount {
    String firstName;
    String lastName;
    String accountNumber;
}
