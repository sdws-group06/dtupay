package com.dtupay.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Sergio Segura (s194726)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestTokensInfo {
    private int amount;
    private String cid;
}
