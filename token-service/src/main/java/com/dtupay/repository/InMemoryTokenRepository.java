package com.dtupay.repository;

import com.dtupay.entities.Token;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: Sergio Segura (s194726)
 */

public class InMemoryTokenRepository implements ITokenRepository {
    private final Map<UUID, List<Token>> tokens = new HashMap<>();

    @Override
    public void addCustomer(UUID cid) {
        tokens.put(cid, new ArrayList<>());
    }

    @Override
    public void addToken(UUID cid, Token token) {
        tokens.get(cid).add(token);
    }

    @Override
    public List<Token> getValidTokens(UUID cid) {
        return tokens.get(cid).stream().filter(t -> !t.isUsed()).collect(Collectors.toList());
    }

    @Override
    public int validTokensCount(UUID id) {
        return getValidTokens(id).size();
    }

    @Override
    public UUID getCustomerIdFromToken(String tokenValue) {
        for(UUID customerId : tokens.keySet()) {
            List<Token> tokenList = tokens.get(customerId);

            for (Token t : tokenList) {
                if(t.toString().equals(tokenValue) && !t.isUsed()) {
                    return customerId;
                }
            }
        }
        return null;
    }


    @Override
    public void updateToken(String token) {
        for(UUID customerId : tokens.keySet()) {

            for (Token t : tokens.get(customerId)) {
                if(t.toString().equals(token)) {
                    t.markedAsUsed();
                }
            }
        }
    }

    public boolean containsCustomerId(UUID id) {
        return tokens.containsKey(id);
    }
}
