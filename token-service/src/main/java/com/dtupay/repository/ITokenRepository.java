package com.dtupay.repository;

import com.dtupay.entities.Token;
import java.util.List;
import java.util.UUID;

/**
 * @author: Khaled Zamzam (s195487)
 */

public interface ITokenRepository {
    void addCustomer(UUID cid);
    void addToken(UUID cid, Token token);
    List<Token> getValidTokens(UUID cid);
    int validTokensCount(UUID cid);
    UUID getCustomerIdFromToken(String tokenValue);

    void updateToken(String token);
}
