package com.dtupay.entities;

import lombok.Data;
import java.io.Serializable;
import java.util.UUID;

/**
 * @author: Khaled Zamzam (s195487)
 */

@Data
public class Token implements Serializable {
    private static final long serialVersionUID = 6197495026351952407L;
    private UUID value;
    private boolean used;

    public static Token generateToken() {
        Token token = new Token();
        token.value = UUID.randomUUID();
        token.used = false;
        return token;
    }

    public void markedAsUsed() {
        this.used = true;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
