package com.dtupay;

import com.dtupay.repository.ITokenRepository;
import com.dtupay.repository.InMemoryTokenRepository;
import com.dtupay.services.TokenService;
import messaging.implementations.RabbitMqQueue;

/**
 * @author: Khaled Zamzam (s195487)
 */

public class StartUp {
    public static void main(String[] args) {
        new StartUp().startup();
    }

    public void startup() {
        RabbitMqQueue mq = new RabbitMqQueue("rabbitMq");
        ITokenRepository repository = new InMemoryTokenRepository();
        new TokenService(repository, mq);
    }
}
