package com.dtupay.exceptions;

/**
 * @author: Khaled Zamzam (s195487)
 */

public class TooManyUnusedTokensException extends Exception {
    public TooManyUnusedTokensException(String message) {
        super(message);
    }
}
