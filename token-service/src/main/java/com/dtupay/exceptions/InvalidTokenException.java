package com.dtupay.exceptions;

/**
 * @author: Sergio Segura (s194726)
 */

public class InvalidTokenException extends Exception {
    public InvalidTokenException(String message) {
        super(message);
    }
}
