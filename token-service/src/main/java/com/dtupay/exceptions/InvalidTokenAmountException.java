package com.dtupay.exceptions;

/**
 * @author: Khaled Zamzam (s195487)
 */

public class InvalidTokenAmountException extends Exception {
    public InvalidTokenAmountException(String message) {
        super(message);
    }
}
