package com.dtupay.services;

import com.dtupay.CorrelationId;
import com.dtupay.data.RequestTokensInfo;
import com.dtupay.exceptions.InvalidTokenAmountException;
import com.dtupay.exceptions.InvalidTokenException;
import com.dtupay.exceptions.TooManyUnusedTokensException;
import com.dtupay.entities.Token;
import com.dtupay.repository.ITokenRepository;
import messaging.Event;
import messaging.MessageQueue;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author: Sergio Segura (s194726)
 */

public class TokenService {
    private final ITokenRepository repository;
    private final MessageQueue queue;

    public TokenService(ITokenRepository repository, MessageQueue queue) {
        this.repository = repository;
        this.queue = queue;
        this.queue.addHandler("CustomerRegistered", this::handleCustomerRegistered);
        this.queue.addHandler("TokensRequested", this::handleTokensRequested);
        this.queue.addHandler("PaymentInitialized", this::handlePaymentInitialized);
        this.queue.addHandler("MoneyTransferred", this::handleMoneyTransferred);
    }

    public List<Token> generateTokens(RequestTokensInfo command) throws InvalidTokenAmountException, TooManyUnusedTokensException {
        int amount = command.getAmount();
        String cid = command.getCid();
        UUID id = UUID.fromString(cid);

        if(!validateTokenAmount(amount)) {
            throw new InvalidTokenAmountException("Invalid amount of tokens requested");
        }

        int validTokenAmount = repository.validTokensCount(id);

        if(validTokenAmount > 1) {
            throw new TooManyUnusedTokensException("Request rejected. Too many unused tokens");
        }

        for(int i = 0; i < amount; i++) {
            repository.addToken(id, Token.generateToken());
        }

        return repository.getValidTokens(id);
    }

    private boolean validateTokenAmount(int amount) {
        return amount >= 1 && amount <= 5;
    }

    public void handleCustomerRegistered(Event e) {
        UUID id = e.getArgument(0, UUID.class);
        repository.addCustomer(id);
    }

    public void handleMoneyTransferred(Event e) {
        String token = e.getArgument(2, String.class);
        repository.updateToken(token);
    }

    public UUID retrieveCustomerIDFromToken(String token) throws InvalidTokenException {
        UUID customerId = repository.getCustomerIdFromToken(token);
        if(customerId == null) {
            throw new InvalidTokenException("The token was not found or has been used");
        }
        return customerId;
    }

    public void handleTokensRequested(Event e) {
        RequestTokensInfo info = e.getArgument(0, RequestTokensInfo.class);
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);

        try {
            List<String> tokens = generateTokens(info).stream().map(Token::toString).collect(Collectors.toList());
            Event event = new Event("TokensRetrieved", new Object[] { tokens, correlationId});
            queue.publish(event);

        } catch (Exception exception) {
            Event event = new Event("TokenRequestError", new Object[] { exception.getMessage(), correlationId});
            queue.publish(event);
        }
    }

    public void handlePaymentInitialized(Event e) {
        String token = e.getArgument(0, String.class);
        UUID paymentId = e.getArgument(1, UUID.class);
        CorrelationId correlationId = e.getArgument(2, CorrelationId.class);

        try {
            UUID customerId = retrieveCustomerIDFromToken(token);
            Event event = new Event("TokenValidated", new Object[] { customerId, paymentId, correlationId});
            queue.publish(event);
        } catch (Exception exception) {
            Event event = new Event("InvalidToken", new Object[] { exception.getMessage(), paymentId, correlationId});
            queue.publish(event);
        }
    }
}
