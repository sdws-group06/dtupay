package com.dtupay;

import lombok.Value;

import java.util.UUID;

/**
 * @author: Sergio Segura (s194726)
 */

@Value
public class CorrelationId {
    UUID id;
    public static CorrelationId randomId() {
        return new CorrelationId(UUID.randomUUID());
    }
}
