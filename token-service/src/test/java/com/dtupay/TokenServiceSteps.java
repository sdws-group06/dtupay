package com.dtupay;

import com.dtupay.data.RequestTokensInfo;
import com.dtupay.entities.Token;
import com.dtupay.repository.InMemoryTokenRepository;
import com.dtupay.services.TokenService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author: Khaled Zamzam (s195487)
 */

public class TokenServiceSteps {
    String customerId;
    MessageQueue queue = mock(MessageQueue.class);
    InMemoryTokenRepository repository = new InMemoryTokenRepository();
    TokenService service = new TokenService(repository, queue);
    Token token;
    CorrelationId correlationId;
    UUID paymentId;

    @Given("a customer with id {string} is registered")
    public void aCustomerWithIdIsRegistered(String customerId) {
        this.customerId = customerId;

        Event event = new Event("CustomerRegistered", new Object[] { customerId });
        service.handleCustomerRegistered(event);
    }

    @And("the customer is stored in the storage")
    public void theCustomerIsStoredInTheStorage() {
        assertTrue(repository.containsCustomerId(UUID.fromString(customerId)));
    }


    @When("this customer requests {int} tokens")
    public void thisCustomerRequestsTokens(int amount) {
        RequestTokensInfo info = new RequestTokensInfo();
        correlationId = CorrelationId.randomId();
        info.setAmount(amount);
        info.setCid(customerId);

        Event event = new Event("TokensRequested", new Object[] { info, correlationId });
        service.handleTokensRequested(event);
    }

    @Then("the tokens are successfully generated and sent")
    public void theTokensAreSuccessfullyGeneratedAndSent() {
        List<String> tokens = repository.getValidTokens(UUID.fromString(customerId)).stream().map(Token::toString).collect(Collectors.toList());

        Event event = new Event("TokensRetrieved", new Object[] { tokens, correlationId});

        verify(queue).publish(event);
    }

    @Then("the error message {string} is sent")
    public void theErrorMessageIsSent(String errorMessage) {
        Event event = new Event("TokenRequestError", new Object[] { errorMessage, correlationId});
        verify(queue).publish(event);
    }

    @And("the customer has {int} unused tokens")
    public void theCustomerHasUnusedToken(int amount) {
        for(int i = 0; i < amount; i++) {
            repository.addToken(UUID.fromString(customerId), Token.generateToken());
        }
    }

    @And("the customer has a valid token")
    public void theCustomerHasAValidToken() {
        token = Token.generateToken();
        repository.addToken(UUID.fromString(customerId), token);
    }

    @When("a payment is initialized")
    public void aPaymentIsInitialized() {
        paymentId = UUID.randomUUID();
        correlationId = CorrelationId.randomId();

        Event event = new Event("PaymentInitialized", new Object[] { token.toString(), paymentId, correlationId });

        service.handlePaymentInitialized(event);
    }

    @Then("the token is successfully validated and the customer id is sent")
    public void theTokenIsSuccessfullyValidatedAndTheCustomerIdIsSent() {
        Event event = new Event("TokenValidated", new Object[] { UUID.fromString(customerId), paymentId, correlationId});
        verify(queue).publish(event);
    }


    @Given("the money has been transferred")
    public void theMoneyHasBeenTransferred() {
        Event event = new Event("MoneyTransferred", new Object[] { correlationId, paymentId, token.toString()});
        service.handleMoneyTransferred(event);
    }

    @Then("the token is marked as used")
    public void theTokenIsMarkedAsUsed() {
        assertTrue(token.isUsed());
    }

    @And("the customer has used token")
    public void theCustomerHasUsedToken() {
        token = Token.generateToken();
        token.markedAsUsed();
        repository.addToken(UUID.fromString(customerId), token);
    }

    @Then("the token is invalidated and the error message {string} is sent")
    public void theTokenIsInvalidatedAndTheErrorMessageIsSent(String errorMessage) {
        Event event = new Event("InvalidToken", new Object[] { errorMessage, paymentId, correlationId});
        verify(queue).publish(event);
    }
}

