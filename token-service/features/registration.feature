Feature: Token feature
  Scenario: Token Request Successful
    Given a customer with id "e87793b0-ceca-4b58-ba42-67b97cdbed62" is registered
    And the customer is stored in the storage
    And the customer has 1 unused tokens
    When this customer requests 5 tokens
    Then the tokens are successfully generated and sent

  Scenario: Too many tokens requested
    Given a customer with id "e87793b0-ceca-4b58-ba42-67b97cdbed62" is registered
    And the customer is stored in the storage
    When this customer requests 7 tokens
    Then the error message "Invalid amount of tokens requested" is sent

  Scenario: Too many unused tokens
    Given a customer with id "e87793b0-ceca-4b58-ba42-67b97cdbed62" is registered
    And the customer is stored in the storage
    And the customer has 3 unused tokens
    When this customer requests 2 tokens
    Then the error message "Request rejected. Too many unused tokens" is sent

  Scenario: Successful payment token process
    Given a customer with id "e87793b0-ceca-4b58-ba42-67b97cdbed62" is registered
    And the customer is stored in the storage
    And the customer has a valid token
    When a payment is initialized
    Then the token is successfully validated and the customer id is sent
    Given the money has been transferred
    Then the token is marked as used

  Scenario: Invalid token used
    Given a customer with id "e87793b0-ceca-4b58-ba42-67b97cdbed62" is registered
    And the customer is stored in the storage
    And the customer has used token
    When a payment is initialized
    Then the token is invalidated and the error message "The token was not found or has been used" is sent