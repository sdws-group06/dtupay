Feature: Payment feature
  Scenario: Payment successful
    Given a payment is requested
    Then the payment is initialized
    When the token is validated
    Then the customer id is received and bank account numbers are requested
    When the bank account numbers are assigned
    Then the money is transferred successfully

  Scenario: Payment unsuccessful
    Given a payment is requested
    Then the payment is initialized
    When the token is validated
    Then the customer id is received and bank account numbers are requested
    When the bank account numbers are assigned
    Then the money transferring is not successful

  Scenario: Invalid token error
    Given a payment is requested
    Then the payment is initialized
    When the token is invalid
    Then send error message