package com.dtupay;

import com.dtupay.data.PaymentInfo;
import com.dtupay.entities.Payment;
import com.dtupay.repositories.PaymentRepository;
import com.dtupay.services.PaymentService;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException;
import dtu.ws.fastmoney.BankServiceException_Exception;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import java.math.BigDecimal;
import java.util.Collection;

import static org.mockito.Mockito.*;

/**
 * @author: Khaled Zamzam (s195487)
 */

public class PaymentSteps {
    MessageQueue queue = mock(MessageQueue.class);
    BankService bank = mock(BankService.class);
    CorrelationId correlationId;
    PaymentRepository repository = new PaymentRepository();
    PaymentService service = new PaymentService(queue, bank, repository);
    Payment payment;

    String customerBankAccountNumber;
    String merchantBankAccountNumber;

    @Given("a payment is requested")
    public void aPaymentIsRequested() {
        PaymentInfo info = new PaymentInfo();
        info.setAmount(200);
        info.setToken("75af1e2d-0848-4580-9351-216e45a6319e");
        info.setMid("aefffd31-7698-4f35-9e32-6c83cf3dc615");
        correlationId = CorrelationId.randomId();
        Event event = new Event("PaymentRequested", new Object[] { info, correlationId});
        service.handlePaymentRequested(event);
    }

    @Then("the payment is initialized")
    public void thePaymentIsInitialized() {
        Collection<Payment> payments = repository.getPayments().values();
        payment = payments.stream().findFirst().get();

        Event event = new Event("PaymentInitialized", new Object[] { payment.getToken(), payment.getPaymentId(), correlationId});
        verify(queue).publish(event);
    }

    @When("the token is validated")
    public void theTokenIsValidated() {
        String customerId = "dcffbd31-7698-4f35-2e37-8c83cf3dc789";
        Event event = new Event("TokenValidated", new Object[] { customerId, payment.getPaymentId(), correlationId});
        service.handleTokenValidated(event);
    }

    @Then("the customer id is received and bank account numbers are requested")
    public void theCustomerIdIsReceivedAndBankAccountNumbersAreRequested() {
        Event event = new Event("BankAccountNumbersRequested", new Object[] { payment.getCustomerId(), payment.getMerchantId(), payment.getPaymentId(), correlationId});
        verify(queue).publish(event);
    }

    @When("the bank account numbers are assigned")
    public void theBankAccountNumbersAreAssigned() {
        customerBankAccountNumber = "74c528ad-aabb-4a06-b16d-edc5733c8c9f";
        merchantBankAccountNumber = "73c528ad-aabb-4a04-b16d-edc5733c8c8f";

        Event event = new Event("BankAccountNumbersAssigned", new Object[] { customerBankAccountNumber, merchantBankAccountNumber, payment.getPaymentId(), correlationId});

        service.handleBankAccountNumbersAssigned(event);
    }

    @Then("the money is transferred successfully")
    public void theBankServiceTransferMoney() throws BankServiceException_Exception {
        verify(bank).transferMoneyFromTo(customerBankAccountNumber, merchantBankAccountNumber, payment.getAmount(), "Payment");
        Event event = new Event("MoneyTransferred", new Object[] { payment.getPaymentId(), correlationId, payment.getToken(), payment});
        verify(queue).publish(event);
    }

    @Then("the money transferring is not successful")
    public void theMoneyTransferringIsNotSuccessful() throws BankServiceException_Exception {
        String errorMessage = "Transfer failed";
        doThrow(new BankServiceException_Exception(errorMessage, new BankServiceException()))
                .when(bank)
                .transferMoneyFromTo("", "", BigDecimal.ONE, "");

        Event event = new Event("PaymentError", new Object[] { errorMessage, correlationId });
        queue.publish(event);
    }

    @When("the token is invalid")
    public void theTokenIsInvalid() {
        String errorMessage = "Token already used";

        Event event = new Event("InvalidToken", new Object[] { errorMessage, payment.getPaymentId(), correlationId });
        service.handleError(event);
    }

    @Then("send error message")
    public void sendErrorMessage() {
        String errorMessage = "Token already used";
        Event event = new Event("PaymentError", new Object[] { errorMessage, correlationId });
        verify(queue).publish(event);
    }
}

