package com.dtupay;

import com.dtupay.repositories.IPaymentRepository;
import com.dtupay.repositories.PaymentRepository;
import com.dtupay.services.PaymentService;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import messaging.implementations.RabbitMqQueue;

/**
 * @author: Khaled Zamzam (s195487)
 */

public class StartUp {

    public static void main(String[] args) {
        new StartUp().startup();
    }

    public void startup() {
        RabbitMqQueue mq = new RabbitMqQueue("rabbitMq");
        BankService bank = new BankServiceService().getBankServicePort();
        IPaymentRepository repository = new PaymentRepository();
        new PaymentService(mq, bank, repository);
    }
}
