package com.dtupay.data;

import lombok.Data;

/**
 * @author: Sergio Segura (s194726)
 */

@Data
public class PaymentInfo {
    String token;
    String mid;
    int amount;
}
