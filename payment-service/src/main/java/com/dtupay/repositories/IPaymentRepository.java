package com.dtupay.repositories;

import com.dtupay.entities.Payment;
import java.util.UUID;

/**
 * @author: Khaled Zamzam (s195487)
 */

public interface IPaymentRepository {
    void addPayment(Payment payment);

    Payment getPayment(UUID paymentId);

    void removePayment(UUID paymentId);
}
