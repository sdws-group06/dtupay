package com.dtupay.repositories;

import com.dtupay.entities.Payment;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Sergio Segura (s194726)
 */

public class PaymentRepository implements IPaymentRepository {
    private final Map<UUID, Payment> payments = new ConcurrentHashMap<>();

    @Override
    public void addPayment(Payment payment) {
        payments.put(payment.getPaymentId(), payment);
    }

    @Override
    public Payment getPayment(UUID paymentId) {
        return payments.get(paymentId);
    }

    @Override
    public void removePayment(UUID paymentId) {
        payments.remove(paymentId);
    }

    public Map<UUID, Payment> getPayments() {
        return payments;
    }
}
