package com.dtupay.entities;

import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author: Khaled Zamzam (s195487)
 */

@Getter
public class Payment {
    private final UUID paymentId;

    @Setter
    private String customerId;

    @Setter
    private String merchantId;

    @Setter
    private BigDecimal amount;

    @Setter
    private String token;

    public Payment() {
        this.paymentId = UUID.randomUUID();
    }
}
