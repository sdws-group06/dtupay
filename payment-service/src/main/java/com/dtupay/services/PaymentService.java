package com.dtupay.services;

import com.dtupay.CorrelationId;
import com.dtupay.data.PaymentInfo;
import com.dtupay.entities.Payment;
import com.dtupay.repositories.IPaymentRepository;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import messaging.Event;
import messaging.MessageQueue;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author: Sergio Segura (s194726)
 */

public class PaymentService {
    private final MessageQueue queue;
    private final BankService bank;
    private final IPaymentRepository repository;

    public PaymentService(MessageQueue queue, BankService bank, IPaymentRepository repository) {
        this.queue = queue;
        this.bank = bank;
        this.repository = repository;
        queue.addHandler("PaymentRequested", this::handlePaymentRequested);
        queue.addHandler("TokenValidated", this::handleTokenValidated);
        queue.addHandler("InvalidToken", this::handleError);
        queue.addHandler("BankAccountNumbersAssigned", this::handleBankAccountNumbersAssigned);
    }

    private void transferMoney(String customerBankAccountNumber, String merchantBankAccountNumber, BigDecimal amount) throws BankServiceException_Exception {
        bank.transferMoneyFromTo(customerBankAccountNumber, merchantBankAccountNumber, amount, "Payment");
    }

    public void handlePaymentRequested(Event e) {
        var info = e.getArgument(0, PaymentInfo.class);
        var correlationId = e.getArgument(1, CorrelationId.class);

        Payment payment = new Payment();
        payment.setAmount(BigDecimal.valueOf(info.getAmount()));
        payment.setMerchantId(info.getMid());
        payment.setToken(info.getToken());
        repository.addPayment(payment);

        Event event = new Event("PaymentInitialized", new Object[] { payment.getToken(), payment.getPaymentId(), correlationId});
        queue.publish(event);
    }

    public void handleTokenValidated(Event e) {
        var customerId = e.getArgument(0, String.class);
        var paymentId = e.getArgument(1, UUID.class);
        var correlationId = e.getArgument(2, CorrelationId.class);

        Payment payment = repository.getPayment(paymentId);
        payment.setCustomerId(customerId);

        Event event = new Event("BankAccountNumbersRequested", new Object[] { payment.getCustomerId(), payment.getMerchantId(), payment.getPaymentId(), correlationId});
        queue.publish(event);
    }

    public void handleBankAccountNumbersAssigned(Event e) {
        var customerBankAccountNumber = e.getArgument(0, String.class);
        var merchantBankAccountNumber = e.getArgument(1, String.class);
        var paymentId = e.getArgument(2, UUID.class);
        var correlationId = e.getArgument(3, CorrelationId.class);
        Payment payment = repository.getPayment(paymentId);

        try {
            transferMoney(customerBankAccountNumber, merchantBankAccountNumber, payment.getAmount());
            Event event = new Event("MoneyTransferred", new Object[] { payment.getPaymentId(), correlationId, payment.getToken(), payment});
            queue.publish(event);
        } catch (BankServiceException_Exception exception) {
            Event event = new Event("PaymentError", new Object[] { exception.getMessage(), correlationId });
            queue.publish(event);
        }
    }

    public void handleError(Event e) {
        var errorMessage = e.getArgument(0, String.class);
        var paymentId = e.getArgument(1, UUID.class);
        var correlationId = e.getArgument(2, CorrelationId.class);

        repository.removePayment(paymentId);

        Event event = new Event("PaymentError", new Object[] { errorMessage, correlationId });
        queue.publish(event);
    }
}
