#!/bin/bash

set -e

pushd end-to-end-test
mvn clean test
popd
