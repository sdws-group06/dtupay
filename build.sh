#!/bin/bash

set -e

pushd messaging-utilities-3.4
./build.sh
popd

pushd account-service
mvn clean package
docker-compose build
popd

pushd token-service
mvn clean package
docker-compose build
popd

pushd payment-service
mvn clean package
docker-compose build
popd

pushd report-service
mvn clean package
docker-compose build
popd

pushd dtu-pay-service
mvn clean package
docker-compose build
popd