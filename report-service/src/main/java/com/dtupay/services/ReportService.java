package com.dtupay.services;

import com.dtupay.entities.Payment;
import com.dtupay.repositories.interfaces.IReportReadRepository;
import com.dtupay.repositories.interfaces.IReportWriteRepository;
import com.dtupay.valueobjects.CorrelationId;
import messaging.Event;
import messaging.MessageQueue;
import java.util.HashSet;

/**
 * @author: Khaled Zamzam (s195487)
 */

public class ReportService {
    private final MessageQueue queue;
    private final IReportWriteRepository writeRepository;
    private final IReportReadRepository readRepository;

    public ReportService(MessageQueue queue, IReportWriteRepository writeRepo, IReportReadRepository readRepo) {
        this.queue = queue;
        this.writeRepository = writeRepo;
        this.readRepository = readRepo;
        this.queue.addHandler("MoneyTransferred", this::handleMoneyTransferred);
        this.queue.addHandler("CustomerReportRequested", this::handleCustomerReportRequested);
        this.queue.addHandler("MerchantReportRequested", this::handleMerchantReportRequested);
    }

    public void handleMoneyTransferred(Event e) {
        var payment = e.getArgument(3, Payment.class);
        writeRepository.save(payment);
    }

    public void handleCustomerReportRequested(Event e) {
        var customerId = e.getArgument(0, String.class);
        var correlationId = e.getArgument(1, CorrelationId.class);
        var customerReports = readRepository.getCustomerReports(customerId);
        Event event = new Event("CustomerReportRetrieved", new Object[] { customerReports == null ? new HashSet<>() :customerReports, correlationId});
        queue.publish(event);
    }

    public void handleMerchantReportRequested(Event e) {
        var merchantId = e.getArgument(0, String.class);
        var correlationId = e.getArgument(1, CorrelationId.class);
        var merchantReports = readRepository.getMerchantReports(merchantId);
        Event event = new Event("MerchantReportRetrieved", new Object[] { merchantReports == null ? new HashSet<>() : merchantReports, correlationId});
        queue.publish(event);
    }
}
