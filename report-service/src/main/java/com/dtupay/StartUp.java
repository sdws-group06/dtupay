package com.dtupay;

import com.dtupay.repositories.InMemoryReportReadRepository;
import com.dtupay.repositories.InMemoryReportWriteRepository;
import com.dtupay.repositories.interfaces.IReportWriteRepository;
import com.dtupay.services.ReportService;
import messaging.implementations.RabbitMqQueue;

/**
 * @author: Sergio Segura (s194726)
 */

public class StartUp {

    public static void main(String[] args) {
        new StartUp().startUp();
    }

    private void startUp() {
        RabbitMqQueue mq = new RabbitMqQueue("rabbitMq");
        IReportWriteRepository writeRepo = new InMemoryReportWriteRepository(mq);
        InMemoryReportReadRepository readRepo = new InMemoryReportReadRepository(mq);
        new ReportService(mq, writeRepo, readRepo);
    }
}
