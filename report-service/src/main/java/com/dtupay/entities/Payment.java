package com.dtupay.entities;
import lombok.Data;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author: Sergio Segura (s194726)
 */
@Data
public class Payment {
    private UUID paymentId;
    private String customerId;
    private String merchantId;
    private BigDecimal amount;
    private String token;
}
