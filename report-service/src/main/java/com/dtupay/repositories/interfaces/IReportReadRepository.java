package com.dtupay.repositories.interfaces;

import com.dtupay.valueobjects.CustomerReport;
import com.dtupay.valueobjects.MerchantReport;
import java.util.Set;

/**
 * @author: Khaled Zamzam (s195487)
 */

public interface IReportReadRepository {
    Set<CustomerReport> getCustomerReports(String customerId);
    Set<MerchantReport> getMerchantReports(String merchantId);
}
