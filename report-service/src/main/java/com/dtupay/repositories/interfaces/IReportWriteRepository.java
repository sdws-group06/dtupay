package com.dtupay.repositories.interfaces;

import com.dtupay.entities.Payment;

/**
 * @author: Sergio Segura (s194726)
 */

public interface IReportWriteRepository {
    void save(Payment report);
}
