package com.dtupay.repositories;

import com.dtupay.entities.Payment;
import com.dtupay.repositories.interfaces.IReportWriteRepository;
import com.dtupay.valueobjects.Report;
import lombok.Getter;
import messaging.Event;
import messaging.MessageQueue;
import java.util.*;

/**
 * @author: Sergio Segura (s194726)
 */

public class InMemoryReportWriteRepository implements IReportWriteRepository {
    private final MessageQueue queue;
    private final Map<UUID, Payment> payments = new HashMap<>();

    @Getter
    private final Set<Report> reports = new HashSet<>();

    public InMemoryReportWriteRepository(MessageQueue queue) {
        this.queue = queue;
    }

    @Override
    public void save(Payment payment) {
        payments.put(payment.getPaymentId(), payment);
        Report report = new Report();
        report.setAmount(payment.getAmount());
        report.setCustomerId(payment.getCustomerId());
        report.setMerchantId(payment.getMerchantId());
        report.setToken(payment.getToken());
        reports.add(report);
        Event event = new Event("ReportCreated", new Object[]{ report });
        queue.publish(event);
    }
}
