package com.dtupay.repositories;

import com.dtupay.repositories.interfaces.IReportReadRepository;
import com.dtupay.valueobjects.CustomerReport;
import com.dtupay.valueobjects.MerchantReport;
import com.dtupay.valueobjects.Report;
import messaging.Event;
import messaging.MessageQueue;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author: Khaled Zamzam (s195487)
 */

public class InMemoryReportReadRepository implements IReportReadRepository {
    private final Map<String, Set<CustomerReport>> customerReports = new HashMap<>();
    private final Map<String, Set<MerchantReport>> merchantReports = new HashMap<>();

    public InMemoryReportReadRepository(MessageQueue queue) {
        queue.addHandler("ReportCreated", this::apply);
    }

    private void apply(Event e) {
        Report report = e.getArgument(0, Report.class);

        CustomerReport customerReport = new CustomerReport(report.getToken(), report.getMerchantId(), report.getAmount());
        MerchantReport merchantReport = new MerchantReport(report.getToken(), report.getAmount());

        var reportsByCustomer = customerReports.getOrDefault(report.getCustomerId(), new HashSet<>());
        reportsByCustomer.add(customerReport);

        var reportsByMerchant = merchantReports.getOrDefault(report.getMerchantId(), new HashSet<>());
        reportsByMerchant.add(merchantReport);

        customerReports.put(report.getCustomerId(), reportsByCustomer);
        merchantReports.put(report.getMerchantId(), reportsByMerchant);
    }

    @Override
    public Set<CustomerReport> getCustomerReports(String customerId) {
        return customerReports.get(customerId);
    }

    @Override
    public Set<MerchantReport> getMerchantReports(String merchantId) {
        return merchantReports.get(merchantId);
    }
}
