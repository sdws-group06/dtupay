package com.dtupay.valueobjects;

import lombok.AllArgsConstructor;
import java.math.BigDecimal;

/**
 * @author: Sergio Segura (s194726)
 */

@AllArgsConstructor
public class MerchantReport {
    private String token;
    private BigDecimal amount;
}
