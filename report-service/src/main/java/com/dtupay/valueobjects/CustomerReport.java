package com.dtupay.valueobjects;

import lombok.AllArgsConstructor;
import java.math.BigDecimal;

/**
 * @author: Khaled Zamzam (s195487)
 */
@AllArgsConstructor
public class CustomerReport {
    private String token;
    private String merchantId;
    private BigDecimal amount;
}
