package com.dtupay.valueobjects;

import lombok.Data;
import java.math.BigDecimal;

/**
 * @author: Khaled Zamzam (s195487)
 */
@Data
public class Report {
    private String merchantId;
    private String customerId;
    private BigDecimal amount;
    private String token;
}
