package com.dtupay;

import com.dtupay.entities.Payment;
import com.dtupay.repositories.InMemoryReportReadRepository;
import com.dtupay.repositories.InMemoryReportWriteRepository;
import com.dtupay.repositories.interfaces.IReportReadRepository;
import com.dtupay.repositories.interfaces.IReportWriteRepository;
import com.dtupay.services.ReportService;
import com.dtupay.valueobjects.CorrelationId;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import messaging.implementations.MessageQueueSync;
import java.math.BigDecimal;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author: Sergio Segura (s194726)
 */

public class ReportServiceSteps {
    MessageQueue queue = new MessageQueueSync();
    MessageQueue queueExternal = mock(MessageQueue.class);
    IReportWriteRepository writeRepo = new InMemoryReportWriteRepository(queue);
    IReportReadRepository readRepo = new InMemoryReportReadRepository(queue);
    ReportService service = new ReportService(queueExternal, writeRepo, readRepo);
    Payment payment;
    String customerId = "cid1";
    String merchantId = "mid1";
    String token = "token1";
    CorrelationId correlationId;

    @Given("the MoneyTransferred event is received")
    public void theMoneyTransferredEventIsReceived() {
        payment = new Payment();
        payment.setPaymentId(UUID.randomUUID());
        payment.setAmount(BigDecimal.ONE);
        payment.setCustomerId(customerId);
        payment.setMerchantId(merchantId);
        payment.setToken(token);
        Event event = new Event("MoneyTransferred", new Object[] { "", "", "", payment });
        service.handleMoneyTransferred(event);
    }

    @When("the {string} event is received")
    public void theCustomerReportRequestedIsReceived(String eventName) {
        correlationId = CorrelationId.randomId();

        if(eventName.equals("CustomerReportRequested")) {
            service.handleCustomerReportRequested(new Event(eventName, new Object[] { customerId, correlationId}));
        } else {
            service.handleMerchantReportRequested(new Event(eventName, new Object[] { merchantId, correlationId}));
        }
    }

    @Then("the {string} event is sent")
    public void theEventIsSent(String eventName) {

        if(eventName.equals("CustomerReportRetrieved")) {
            var customerReports = readRepo.getCustomerReports(customerId);

            Event event = new Event(eventName, new Object[] { customerReports, correlationId});
            verify(queueExternal).publish(event);
        } else {
            var merchantReports = readRepo.getMerchantReports(merchantId);

            Event event = new Event(eventName, new Object[] { merchantReports, correlationId});
            verify(queueExternal).publish(event);
        }
    }
}
