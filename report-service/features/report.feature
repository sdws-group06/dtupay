Feature: Report feature
  Scenario: Customer report request
    Given the MoneyTransferred event is received
    When the "CustomerReportRequested" event is received
    Then the "CustomerReportRetrieved" event is sent

  Scenario: Merchant report request
    Given the MoneyTransferred event is received
    When the "MerchantReportRequested" event is received
    Then the "MerchantReportRetrieved" event is sent

