package com.dtupay;

import com.dtupay.exceptions.ConflictException;
import com.dtupay.models.RegisterRequest;
import com.dtupay.services.CustomerService;
import com.dtupay.valueobjects.CorrelationId;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.Consumer;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class CustomerSteps {
    private final Map<String,CompletableFuture<Event>> publishedEvents = new HashMap<>();
    private final MessageQueue queue = new MessageQueue() {
        @Override
        public void publish(Event event) {
            var req = event.getArgument(0, RegisterRequest.class);
            publishedEvents.get(req.getAccountNumber()).complete(event);}
        @Override
        public void addHandler(String s, Consumer<Event> consumer) {}
    };

    private CustomerService service = new CustomerService(queue);
    private CompletableFuture<String> registeredCustomer = new CompletableFuture<>();

    private Map<RegisterRequest,CorrelationId> correlationIds = new HashMap<>();

    private RegisterRequest req;

    @Given("a customer registration request")
    public void aCustomerRegistrationRequest() {
        req = new RegisterRequest("John", "Doe", "123");
        publishedEvents.put(req.getAccountNumber(), new CompletableFuture<>());
    }

    @When("the customer is being registered")
    public void theCustomerIsBeingRegistered() {
        new Thread(() -> {
            try {
                var result = service.register(req);
                registeredCustomer.complete(result);
            } catch (Exception e) {
                registeredCustomer.completeExceptionally(new ConflictException(e.getMessage()));
            }

        }).start();
    }

    @Then("the CustomerRegistrationRequested event is published")
    public void theCustomerRegistrationRequestedEventIsPublished() {
        Event event = publishedEvents.get(req.getAccountNumber()).join();
        var req = event.getArgument(0, RegisterRequest.class);
        var correlationId = event.getArgument(1, CorrelationId.class);
        correlationIds.put(req, correlationId);
    }

    @When("the CustomerRegistered is received")
    public void theCustomerRegisteredIsReceived() {
        String customerId = "cid1";
        Event event = new Event("CustomerRegistered", new Object[] { customerId, correlationIds.get(req)});
        service.handleCustomerRegistered(event);
    }

    @Then("the customer is registered and the customer id is returned")
    public void theCustomerIsRegisteredAndTheCustomerIdIsReturned() {
        assertEquals(registeredCustomer.join(), "cid1");

    }

    @When("the CustomerAlreadyRegistered is received")
    public void theCustomerAlreadyRegisteredIsReceived() {
        String errorMessage = "Account already exist";
        Event event = new Event("CustomerAlreadyRegistered", new Object[] { errorMessage, correlationIds.get(req)});
        service.handleCustomerAlreadyRegistered(event);
    }

    @Then("an exception for the customer is thrown")
    public void anExceptionForTheCustomerIsThrown() {
        assertThrows(CompletionException.class, () -> {
            registeredCustomer.join();
        });
    }
}
