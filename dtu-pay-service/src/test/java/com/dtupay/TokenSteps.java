package com.dtupay;

import com.dtupay.exceptions.ConflictException;
import com.dtupay.models.TokenRequest;
import com.dtupay.services.TokenService;
import com.dtupay.valueobjects.CorrelationId;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class TokenSteps {
    private final Map<String, CompletableFuture<Event>> publishedEvents = new HashMap<>();

    private final MessageQueue queue = new MessageQueue() {
        @Override
        public void publish(Event event) {
            var req = event.getArgument(0, TokenRequest.class);
            publishedEvents.get(req.getCid()).complete(event);
        }
        @Override
        public void addHandler(String s, Consumer<Event> consumer) {}
    };

    private TokenService service = new TokenService(queue);
    private CompletableFuture<List<String>> tokens = new CompletableFuture<>();

    private Map<TokenRequest, CorrelationId> correlationIds = new HashMap<>();

    private TokenRequest req;

    @Given("a token request")
    public void aTokenRequest() {
        req = new TokenRequest(5, "cid1");
        publishedEvents.put(req.getCid(), new CompletableFuture<>());
    }

    @When("tokens are being fetched")
    public void tokensAreBeingFetched() {
        new Thread(() -> {
            try {
                var result = service.getTokens(req);
                tokens.complete(result);
            } catch (Exception e) {
                tokens.completeExceptionally(new ConflictException(e.getMessage()));
            }

        }).start();
    }

    @Then("the TokensRequested event is published")
    public void theTokensRequestedEventIsPublished() {
        Event event = publishedEvents.get(req.getCid()).join();
        var req = event.getArgument(0, TokenRequest.class);
        var correlationId = event.getArgument(1, CorrelationId.class);
        correlationIds.put(req, correlationId);
    }

    @When("the TokensRetrieved event is received")
    public void theTokensRetrievedEventIsReceived() {
        List<String> tokenList = new ArrayList<>();
        tokenList.add("token1");
        Event event = new Event("TokensRetrieved", new Object[] { tokenList, correlationIds.get(req)});
        service.handleTokensRetrieved(event);
    }

    @Then("a list of valid tokens is returned")
    public void aListOfValidTokensIsReturned() {
        List<String> tokenList = new ArrayList<>();
        tokenList.add("token1");
        assertEquals(tokens.join(), tokenList);
    }

    @When("the TokenRequestError event is received")
    public void theTokenRequestErrorEventIsReceived() {
        String errorMessage = "Token error";
        Event event = new Event("TokenRequestError", new Object[] { errorMessage, correlationIds.get(req)});
        service.handleTokenRequestError(event);
    }

    @Then("a token exception is thrown")
    public void aTokenExceptionIsThrown() {
        assertThrows(CompletionException.class, () -> {
            tokens.join();
        });
    }
}
