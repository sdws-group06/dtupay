package com.dtupay;

import com.dtupay.models.PaymentRequest;
import com.dtupay.services.PaymentService;
import com.dtupay.valueobjects.CorrelationId;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jakarta.ws.rs.BadRequestException;
import messaging.Event;
import messaging.MessageQueue;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class PaymentSteps {

    private final Map<String, CompletableFuture<Event>> publishedEvents = new HashMap<>();

    private final MessageQueue queue = new MessageQueue() {
        @Override
        public void publish(Event event) {
            var req = event.getArgument(0, PaymentRequest.class);
            publishedEvents.get(req.getMid()).complete(event);
        }
        @Override
        public void addHandler(String s, Consumer<Event> consumer) {}
    };

    private PaymentService service = new PaymentService(queue);
    private CompletableFuture<String> payment = new CompletableFuture<>();

    private Map<PaymentRequest, CorrelationId> correlationIds = new HashMap<>();

    private PaymentRequest req;


    @Given("a payment request")
    public void aPaymentRequest() {
        req = new PaymentRequest("token", "mid1", 100);
        publishedEvents.put(req.getMid(), new CompletableFuture<>());

    }

    @When("a payment is being paid")
    public void aPaymentIsBeingPaid() {
        new Thread(() -> {
            try {
                var result = service.pay(req);
                payment.complete(result);
            } catch (Exception e) {
                payment.completeExceptionally(new BadRequestException(e.getMessage()));
            }
        }).start();
    }

    @Then("the PaymentRequested event is published")
    public void thePaymentRequestedEventIsPublished() {
        Event event = publishedEvents.get(req.getMid()).join();
        var req = event.getArgument(0, PaymentRequest.class);
        var correlationId = event.getArgument(1, CorrelationId.class);
        correlationIds.put(req, correlationId);
    }

    @When("the MoneyTransferred event is received")
    public void theMoneyTransferredEventIsReceived() {
        String paymentId = "pid1";
        Event event = new Event("MoneyTransferred", new Object[] { paymentId, correlationIds.get(req)});
        service.handleMoneyTransferred(event);
    }

    @Then("the payment id is returned")
    public void thePaymentIdIsReturned() {
        assertEquals(payment.join(), "pid1");
    }

    @When("the PaymentError event is received")
    public void thePaymentErrorEventIsReceived() {
        String errorMessage = "Payment error";
        Event event = new Event("PaymentError", new Object[] { errorMessage, correlationIds.get(req)});
        service.handlePaymentError(event);
    }

    @Then("a payment exception is thrown")
    public void aPaymentExceptionIsThrown() {
        assertThrows(CompletionException.class, () -> {
            payment.join();
        });
    }
}
