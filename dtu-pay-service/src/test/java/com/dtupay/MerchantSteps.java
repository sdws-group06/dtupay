package com.dtupay;

import com.dtupay.exceptions.ConflictException;
import com.dtupay.models.RegisterRequest;
import com.dtupay.services.MerchantService;
import com.dtupay.valueobjects.CorrelationId;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author: Sergio Segura (s194726)
 */
public class MerchantSteps {
    private final Map<String, CompletableFuture<Event>> publishedEvents = new HashMap<>();
    private final MessageQueue queue = new MessageQueue() {
        @Override
        public void publish(Event event) {
            var req = event.getArgument(0, RegisterRequest.class);
            publishedEvents.get(req.getAccountNumber()).complete(event);
        }
        @Override
        public void addHandler(String s, Consumer<Event> consumer) {}
    };

    private MerchantService service = new MerchantService(queue);
    private CompletableFuture<String> registeredMerchant = new CompletableFuture<>();

    private Map<RegisterRequest, CorrelationId> correlationIds = new HashMap<>();

    private RegisterRequest req;

    @Given("a merchant registration request")
    public void aMerchantRegistrationRequest() {
        req = new RegisterRequest("John", "Doe", "123");
        publishedEvents.put(req.getAccountNumber(), new CompletableFuture<>());
    }

    @When("the merchant is being registered")
    public void theMerchantIsBeingRegistered() {
        new Thread(() -> {
            try {
                var result = service.register(req);
                registeredMerchant.complete(result);
            } catch (Exception e) {
                registeredMerchant.completeExceptionally(new ConflictException(e.getMessage()));
            }

        }).start();
    }

    @Then("the MerchantRegistrationRequested event is published")
    public void theMerchantRegistrationRequestedEventIsPublished() {
        Event event = publishedEvents.get(req.getAccountNumber()).join();
        var req = event.getArgument(0, RegisterRequest.class);
        var correlationId = event.getArgument(1, CorrelationId.class);
        correlationIds.put(req, correlationId);
    }

    @When("the MerchantRegistered is received")
    public void theMerchantRegisteredIsReceived() {
        String merchantId = "mid1";
        Event event = new Event("MerchantRegistered", new Object[] { merchantId, correlationIds.get(req)});
        service.handleMerchantRegistered(event);
    }

    @Then("the merchant is registered and the merchant id is returned")
    public void theMerchantIsRegisteredAndTheMerchantIdIsReturned() {
        assertEquals(registeredMerchant.join(), "mid1");

    }

    @When("the MerchantAlreadyRegistered is received")
    public void theMerchantAlreadyRegisteredIsReceived() {
        String errorMessage = "Account already exist";
        Event event = new Event("MerchantAlreadyRegistered", new Object[] { errorMessage, correlationIds.get(req)});
        service.handleMerchantAlreadyRegistered(event);
    }

    @Then("an exception for the merchant is thrown")
    public void anExceptionForTheMerchantIsThrown() {
        assertThrows(CompletionException.class, () -> {
            registeredMerchant.join();
        });
    }
}
