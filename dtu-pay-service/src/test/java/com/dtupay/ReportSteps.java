package com.dtupay;

import com.dtupay.models.CustomerReport;
import com.dtupay.models.MerchantReport;
import com.dtupay.services.ReportService;
import com.dtupay.valueobjects.CorrelationId;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author: Sergio Segura (s194726)
 */
public class ReportSteps {

    private final Map<String, CompletableFuture<Event>> publishedEvents = new HashMap<>();

    private final MessageQueue queue = new MessageQueue() {
        @Override
        public void publish(Event event) {
            var id = event.getArgument(0, String.class);
            publishedEvents.get(id).complete(event);
        }
        @Override
        public void addHandler(String s, Consumer<Event> consumer) {}
    };

    private ReportService service = new ReportService(queue);
    private CompletableFuture<Set<CustomerReport>> customerReport = new CompletableFuture<>();

    private CompletableFuture<Set<MerchantReport>> merchantReport = new CompletableFuture<>();

    private Map<String, CorrelationId> correlationIds = new HashMap<>();
    private String customerId;
    private String merchantId;

    @Given("a customer request report")
    public void aCustomerRequestReport() {
        customerId = "cid1";
        publishedEvents.put(customerId, new CompletableFuture<>());
    }

    @Given("a merchant request report")
    public void aMerchantRequestReport() {
        merchantId = "mid1";
        publishedEvents.put(merchantId, new CompletableFuture<>());
    }

    @When("the customer report is being retrieved")
    public void theCustomerReportIsBeingRetrieved() {
        new Thread(() -> {
            var result = service.getCustomerReport(customerId);
            customerReport.complete(result);
        }).start();
    }

    @When("the merchant report is being retrieved")
    public void theMerchantReportIsBeingRetrieved() {
        new Thread(() -> {
            var result = service.getMerchantReport(merchantId);
            merchantReport.complete(result);
        }).start();
    }

    @Then("the CustomerReportRequested event is published")
    public void theCustomerReportRequestedEventIsPublished() {
        Event event = publishedEvents.get(customerId).join();
        var req = event.getArgument(0, String.class);
        var correlationId = event.getArgument(1, CorrelationId.class);
        correlationIds.put(req, correlationId);
    }

    @Then("the MerchantReportRequested event is published")
    public void theMerchantReportRequestedEventIsPublished() {
        Event event = publishedEvents.get(merchantId).join();
        var req = event.getArgument(0, String.class);
        var correlationId = event.getArgument(1, CorrelationId.class);
        correlationIds.put(req, correlationId);
    }

    @When("the CustomerReportRetrieved event is received")
    public void theCustomerReportRetrievedEventIsReceived() {
        Event event = new Event("CustomerReportRetrieved", new Object[] { new HashSet<>(), correlationIds.get(customerId)});
        service.handleCustomerReportRetrieved(event);
    }

    @When("the MerchantReportRetrieved event is received")
    public void theMerchantReportRetrievedEventIsReceived() {
        Event event = new Event("MerchantReportRetrieved", new Object[] { new HashSet<>(), correlationIds.get(merchantId)});
        service.handleMerchantReportRetrieved(event);
    }

    @Then("a set of customer report is returned")
    public void aSetOfCustomerReportIsReturned() {
        assertInstanceOf(Set.class, customerReport.join());
    }

    @Then("a set of merchant report is returned")
    public void aSetOfMerchantReportIsReturned() {
        assertInstanceOf(Set.class, merchantReport.join());
    }
}
