package com.dtupay.exceptions;

import jakarta.ws.rs.ClientErrorException;
import jakarta.ws.rs.core.Response;

/**
 * @author: Sergio Segura (s194726)
 */
public class ConflictException extends ClientErrorException {
    public ConflictException(String message) {
        super(message, Response.Status.CONFLICT);
    }
}
