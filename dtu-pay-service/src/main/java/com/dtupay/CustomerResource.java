package com.dtupay;

import com.dtupay.factory.CustomerServiceFactory;
import com.dtupay.factory.ReportServiceFactory;
import com.dtupay.factory.TokenServiceFactory;
import com.dtupay.models.CustomerReport;
import com.dtupay.models.RegisterRequest;
import com.dtupay.models.TokenRequest;
import com.dtupay.services.CustomerService;
import com.dtupay.services.ReportService;
import com.dtupay.services.TokenService;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.util.List;
import java.util.Set;

/**
 * @author: Khaled Zamzam (s195487)
 */

@Path("customers")
public class CustomerResource {
    CustomerService service = new CustomerServiceFactory().getService();
    TokenService tokenService = new TokenServiceFactory().getService();
    ReportService reportService = new ReportServiceFactory().getService();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String register(RegisterRequest req) {
        return service.register(req);
    }

    @POST
    @Path("tokens")
    @Consumes(MediaType.APPLICATION_JSON)
    public List<String> getTokens(TokenRequest req) {
        return tokenService.getTokens(req);
    }

    @GET
    @Path("report/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<CustomerReport> getTokens(@PathParam("id") String id) {
        return reportService.getCustomerReport(id);
    }
}
