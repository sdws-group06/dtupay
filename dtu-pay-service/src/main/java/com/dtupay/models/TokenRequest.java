package com.dtupay.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Sergio Segura (s194726)
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TokenRequest {
    private int amount;
    private String cid;
}
