package com.dtupay.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Sergio Segura (s194726)
 */

@AllArgsConstructor
@Data
@NoArgsConstructor
public class PaymentRequest {
    private String token;
    private String mid;
    private int amount;
}
