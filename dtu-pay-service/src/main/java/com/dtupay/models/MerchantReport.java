package com.dtupay.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

/**
 * @author: Khaled Zamzam (s195487)
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MerchantReport {
    private String token;
    private BigDecimal amount;
}
