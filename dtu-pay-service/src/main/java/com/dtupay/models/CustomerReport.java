package com.dtupay.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author: Sergio Segura (s194726)
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CustomerReport {
    private String token;
    private String merchantId;
    private BigDecimal amount;
}
