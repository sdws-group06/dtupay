package com.dtupay.factory;

import com.dtupay.services.PaymentService;
import messaging.implementations.RabbitMqQueue;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class PaymentServiceFactory {
    static PaymentService service = null;
    public PaymentService getService() {
        if(service != null) {
            return service;
        }

        var mq = new RabbitMqQueue("rabbitMq");
        return new PaymentService(mq);
    }
}
