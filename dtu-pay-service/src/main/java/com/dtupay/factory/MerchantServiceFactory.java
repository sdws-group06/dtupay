package com.dtupay.factory;

import com.dtupay.services.MerchantService;
import messaging.implementations.RabbitMqQueue;

/**
 * @author: Sergio Segura (s194726)
 */
public class MerchantServiceFactory {
    static MerchantService service = null;

    public MerchantService getService() {
        if(service != null) {
            return service;
        }

        var mq = new RabbitMqQueue("rabbitMq");
        return new MerchantService(mq);
    }
}
