package com.dtupay.factory;

import com.dtupay.services.CustomerService;
import messaging.implementations.RabbitMqQueue;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class CustomerServiceFactory {
    static CustomerService service = null;

    public CustomerService getService() {
        if(service != null) {
            return service;
        }

        var mq = new RabbitMqQueue("rabbitMq");
        service = new CustomerService(mq);
        return service;
    }
}
