package com.dtupay.factory;

import com.dtupay.services.TokenService;
import messaging.implementations.RabbitMqQueue;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class TokenServiceFactory {
    static TokenService service = null;
    public TokenService getService() {
        if(service != null) {
            return service;
        }

        var mq = new RabbitMqQueue("rabbitMq");
        return new TokenService(mq);
    }
}
