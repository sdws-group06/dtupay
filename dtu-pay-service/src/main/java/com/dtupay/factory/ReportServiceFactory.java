package com.dtupay.factory;

import com.dtupay.services.ReportService;
import messaging.implementations.RabbitMqQueue;

/**
 * @author: Sergio Segura (s194726)
 */

public class ReportServiceFactory {

    static ReportService service = null;

    public ReportService getService() {
        if(service != null) {
            return service;
        }

        var mq = new RabbitMqQueue("rabbitMq");
        service = new ReportService(mq);
        return service;
    }
}
