package com.dtupay.services;

import com.dtupay.valueobjects.CorrelationId;
import com.dtupay.exceptions.ConflictException;
import com.dtupay.models.TokenRequest;
import messaging.Event;
import messaging.MessageQueue;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Khaled Zamzam (s195487)
 */

public class TokenService {
    private final MessageQueue queue;
    private final Map<CorrelationId, CompletableFuture<List<String>>> correlations = new ConcurrentHashMap<>();

    public TokenService(MessageQueue queue) {
        this.queue = queue;
        queue.addHandler("TokensRetrieved", this::handleTokensRetrieved);
        queue.addHandler("TokenRequestError", this::handleTokenRequestError);
    }

    public List<String> getTokens(TokenRequest req) {
        try {
            CorrelationId correlationId = CorrelationId.randomId();
            correlations.put(correlationId, new CompletableFuture<>());
            Event event = new Event("TokensRequested", new Object[] { req, correlationId });
            queue.publish(event);
            return correlations.get(correlationId).join();
        } catch (CompletionException e) {
            if (e.getCause() instanceof ConflictException) {
                throw new ConflictException(e.getCause().getMessage());
            }
            throw e;
        }
    }

    public void handleTokensRetrieved(Event e) {
        var tokens = e.getArgument(0, ArrayList.class);
        var correlationId = e.getArgument(1, CorrelationId.class);
        correlations.get(correlationId).complete(tokens);
    }

    public void handleTokenRequestError(Event e) {
        String message = e.getArgument(0, String.class);
        var correlationId = e.getArgument(1, CorrelationId.class);
        correlations.get(correlationId).completeExceptionally(new ConflictException(message));
    }
}
