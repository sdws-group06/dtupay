package com.dtupay.services;

import com.dtupay.valueobjects.CorrelationId;
import com.dtupay.exceptions.ConflictException;
import com.dtupay.models.RegisterRequest;
import messaging.Event;
import messaging.MessageQueue;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Khaled Zamzam (s195487)
 */
public class CustomerService {
    private final MessageQueue queue;
    private final Map<CorrelationId, CompletableFuture<String>> correlations = new ConcurrentHashMap<>();

    public CustomerService(MessageQueue queue) {
        this.queue = queue;
        queue.addHandler("CustomerRegistered", this::handleCustomerRegistered);
        queue.addHandler("CustomerAlreadyRegistered", this::handleCustomerAlreadyRegistered);
    }

    public String register(RegisterRequest req) throws ConflictException {
        try {
            CorrelationId correlationId = CorrelationId.randomId();
            correlations.put(correlationId, new CompletableFuture<>());
            Event event = new Event("CustomerRegistrationRequested", new Object[] { req, correlationId });
            queue.publish(event);
            return correlations.get(correlationId).join();
        } catch (CompletionException e) {
            if (e.getCause() instanceof ConflictException) {
                throw new ConflictException(e.getCause().getMessage());
            }
            throw e;
        }
    }

    public void handleCustomerRegistered(Event e) {
        var customerId = e.getArgument(0, String.class);
        var correlationId = e.getArgument(1, CorrelationId.class);
        correlations.get(correlationId).complete(customerId);
    }

    public void handleCustomerAlreadyRegistered(Event e) {
        String message = e.getArgument(0, String.class);
        var correlationId = e.getArgument(1, CorrelationId.class);
        correlations.get(correlationId).completeExceptionally(new ConflictException(message));
    }
}
