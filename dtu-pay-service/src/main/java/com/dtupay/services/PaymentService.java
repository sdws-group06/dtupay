package com.dtupay.services;

import com.dtupay.valueobjects.CorrelationId;
import com.dtupay.models.PaymentRequest;
import jakarta.ws.rs.BadRequestException;
import messaging.Event;
import messaging.MessageQueue;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Khaled Zamzam (s195487)
 */

public class PaymentService {
    private final MessageQueue queue;
    private final Map<CorrelationId, CompletableFuture<String>> correlations = new ConcurrentHashMap<>();

    public PaymentService(MessageQueue queue) {
        this.queue = queue;
        queue.addHandler("MoneyTransferred", this::handleMoneyTransferred);
        queue.addHandler("PaymentError", this::handlePaymentError);
    }

    public String pay(PaymentRequest req) {
        try {
            CorrelationId correlationId = CorrelationId.randomId();
            correlations.put(correlationId, new CompletableFuture<>());
            Event event = new Event("PaymentRequested", new Object[] { req, correlationId });
            queue.publish(event);
            return correlations.get(correlationId).join();
        } catch (CompletionException e) {
            if (e.getCause() instanceof BadRequestException) {
                throw new BadRequestException(e.getCause().getMessage());
            }
            throw e;
        }
    }

    public void handlePaymentError(Event e) {
        String message = e.getArgument(0, String.class);
        var correlationId = e.getArgument(1, CorrelationId.class);
        correlations.get(correlationId).completeExceptionally(new BadRequestException(message));
    }

    public void handleMoneyTransferred(Event e) {
        var paymentId = e.getArgument(0, String.class);
        var correlationId = e.getArgument(1, CorrelationId.class);
        correlations.get(correlationId).complete(paymentId);
    }
}
