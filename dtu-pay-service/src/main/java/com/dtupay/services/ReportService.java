package com.dtupay.services;

import com.dtupay.models.CustomerReport;
import com.dtupay.models.MerchantReport;
import com.dtupay.valueobjects.CorrelationId;
import messaging.Event;
import messaging.MessageQueue;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Sergio Segura (s194726)
 */

public class ReportService {
    private final MessageQueue queue;
    private final Map<CorrelationId, CompletableFuture<Set<CustomerReport>>> customerCorrelations = new ConcurrentHashMap<>();
    private final Map<CorrelationId, CompletableFuture<Set<MerchantReport>>> merchantCorrelations = new ConcurrentHashMap<>();

    public ReportService(MessageQueue queue) {
        this.queue = queue;
        this.queue.addHandler("CustomerReportRetrieved", this::handleCustomerReportRetrieved);
        this.queue.addHandler("MerchantReportRetrieved", this::handleMerchantReportRetrieved);
    }

    public Set<CustomerReport> getCustomerReport(String customerId) {
        CorrelationId correlationId = CorrelationId.randomId();
        customerCorrelations.put(correlationId, new CompletableFuture<>());

        Event event = new Event("CustomerReportRequested", new Object[] { customerId, correlationId});
        queue.publish(event);
        return customerCorrelations.get(correlationId).join();
    }

    public Set<MerchantReport> getMerchantReport(String merchantId) {
        CorrelationId correlationId = CorrelationId.randomId();
        merchantCorrelations.put(correlationId, new CompletableFuture<>());

        Event event = new Event("MerchantReportRequested", new Object[] { merchantId, correlationId});
        queue.publish(event);
        return merchantCorrelations.get(correlationId).join();
    }

    public void handleCustomerReportRetrieved(Event e) {
        var customerReports = e.getArgument(0, HashSet.class);
        var correlationId = e.getArgument(1, CorrelationId.class);
        customerCorrelations.get(correlationId).complete(customerReports);
    }

    public void handleMerchantReportRetrieved(Event e) {
        var merchantReports = e.getArgument(0, HashSet.class);
        var correlationId = e.getArgument(1, CorrelationId.class);
        merchantCorrelations.get(correlationId).complete(merchantReports);
    }
}
