package com.dtupay;

import com.dtupay.factory.MerchantServiceFactory;
import com.dtupay.factory.PaymentServiceFactory;
import com.dtupay.factory.ReportServiceFactory;
import com.dtupay.models.CustomerReport;
import com.dtupay.models.MerchantReport;
import com.dtupay.models.PaymentRequest;
import com.dtupay.models.RegisterRequest;
import com.dtupay.services.MerchantService;
import com.dtupay.services.PaymentService;
import com.dtupay.services.ReportService;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;

import java.util.Set;

/**
 * @author: Sergio Segura (s194726)
 */
@Path("merchants")
public class MerchantResource {
    MerchantService service = new MerchantServiceFactory().getService();
    PaymentService paymentService = new PaymentServiceFactory().getService();
    ReportService reportService = new ReportServiceFactory().getService();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String register(RegisterRequest req) {
        return service.register(req);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("pay")
    public String createPayment(PaymentRequest req) {
        return paymentService.pay(req);
    }

    @GET
    @Path("report/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<MerchantReport> getTokens(@PathParam("id") String id) {
        return reportService.getMerchantReport(id);
    }
}
