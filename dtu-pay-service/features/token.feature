Feature: Token
  Scenario: Tokens requested
    Given a token request
    When tokens are being fetched
    Then the TokensRequested event is published
    When the TokensRetrieved event is received
    Then a list of valid tokens is returned

  Scenario: Tokens error
    Given a token request
    When tokens are being fetched
    Then the TokensRequested event is published
    When the TokenRequestError event is received
    Then a token exception is thrown