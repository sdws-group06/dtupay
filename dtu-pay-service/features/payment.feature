Feature: Payment
  Scenario: Payment requested
    Given a payment request
    When a payment is being paid
    Then the PaymentRequested event is published
    When the MoneyTransferred event is received
    Then the payment id is returned

  Scenario: Payment error
    Given a payment request
    When a payment is being paid
    Then the PaymentRequested event is published
    When the PaymentError event is received
    Then a payment exception is thrown