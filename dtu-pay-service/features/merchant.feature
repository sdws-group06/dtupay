Feature: Merchant
  Scenario: Merchant Registration successful
    Given a merchant registration request
    When the merchant is being registered
    Then the MerchantRegistrationRequested event is published
    When the MerchantRegistered is received
    Then the merchant is registered and the merchant id is returned

  Scenario: Merchant already exist
    Given a merchant registration request
    When the merchant is being registered
    Then the MerchantRegistrationRequested event is published
    When the MerchantAlreadyRegistered is received
    Then an exception for the merchant is thrown