Feature: Customer
  Scenario: Customer Registration successful
    Given a customer registration request
    When the customer is being registered
    Then the CustomerRegistrationRequested event is published
    When the CustomerRegistered is received
    Then the customer is registered and the customer id is returned

  Scenario: Customer already exist
    Given a customer registration request
    When the customer is being registered
    Then the CustomerRegistrationRequested event is published
    When the CustomerAlreadyRegistered is received
    Then an exception for the customer is thrown