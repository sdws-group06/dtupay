Feature: Report
  Scenario: Customer Report
    Given a customer request report
    When the customer report is being retrieved
    Then the CustomerReportRequested event is published
    When the CustomerReportRetrieved event is received
    Then a set of customer report is returned

  Scenario: Merchant Report
    Given a merchant request report
    When the merchant report is being retrieved
    Then the MerchantReportRequested event is published
    When the MerchantReportRetrieved event is received
    Then a set of merchant report is returned